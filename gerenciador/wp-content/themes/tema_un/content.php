<?php
/**
 * The default template for displaying content.
 *
 * Used for both single and index/archive/author/catagory/search/tag.
 *
 * @package Tema_UN
 * @since 0.0.1
 */
?>

	<?php
		the_title();
		the_content();
	?>
