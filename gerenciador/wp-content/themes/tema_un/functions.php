<?php
/**
 * Logos functions and definitions.
 *
 * Sets up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * For more information on hooks, actions, and filters,
 * see http://codex.wordpress.org/Plugin_API
 *
 * @package Tema_UN
 */

/**
 * Sets content width.
 */

add_action( 'init', 'register_session' );
function register_session(){
	if( !session_id() )
	    session_start();
}

if ( ! isset( $content_width ) ) {
	$content_width = 600;
}

if ( ! function_exists( 'tema_un_setup_features' ) ) {

	/**
	 * Setup theme features.
	 *
	 * @return void
	 */
	function tema_un_setup_features() {

		/**
		 * Add support for multiple languages.
		 */
		load_theme_textdomain( 'tema_un', get_template_directory() . '/languages' );

		/**
		 * Register nav menus.
		 */
		register_nav_menus(
			array(
				'main-menu' => __( 'Main Menu', 'tema_un' )
			)
		);

		/*
		 * Add post_thumbnails suport.
		 */
		add_theme_support( 'post-thumbnails' );

		/**
		 * Add thumbnail  size
		 */
		add_image_size( 'xxxxxx', 'width', 'height', true );

		/**
		 * Add feed link.
		 */
		add_theme_support( 'automatic-feed-links' );

		/**
		 * Support Custom Header.
		 */
		$default = array(
			'width'         => 1920,
			'height'        => 939,
			'flex-height'   => false,
			'flex-width'    => false,
			'header-text'   => false,
			'default-image' => '',
			'uploads'       => true,
		);


		/**
		 * Support The Excerpt on pages.
		 */
		add_post_type_support( 'page', 'excerpt' );

		/**
		 * Switch default core markup for search form, comment form, and comments to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption'
			)
		);

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );
	}
}

add_action( 'after_setup_theme', 'tema_un_setup_features' );

/**
 * Load site scripts.
 *
 * @return void
 */
function tema_un_enqueue_scripts() {
	$template_url = get_template_directory_uri();

	wp_enqueue_style( 'tema_un-normalize', get_template_directory_uri() . '/assets/css/normalize.css', array(), null, 'all' );
	wp_enqueue_style( 'tema_un-bootstrap', get_template_directory_uri() . '/assets/css/bootstrap.min.css', array(), null, 'all' );
	wp_enqueue_style( 'tema_un-font-awesome', get_template_directory_uri() . '/assets/css/font-awesome.min.css', array(), null, 'all' );

	wp_enqueue_style( 'tema_un-generic', get_template_directory_uri() . '/assets/css/generic.css?ver=' . md5(date('i')), array(), null, 'all' );
	wp_enqueue_style( 'tema_un-principal', get_template_directory_uri() . '/assets/css/main.css?ver=' . md5(date('i')), array(), null, 'all' );


	// jQuery.
	wp_enqueue_script( 'jquery' );

	// General scripts.
	// Main.
	wp_enqueue_script( 'tema_un-bootstrap', $template_url . '/assets/js/bootstrap.min.js', array( 'jquery' ), null, true );
	wp_enqueue_script( 'tema_un-html5', $template_url . '/assets/js/html5.js', '', null, true );
	wp_enqueue_script( 'tema_un-main', $template_url . '/assets/js/main.js?ver=' . md5(date('i')), array( 'jquery' ), null, true );
	wp_localize_script( 'tema_un-main', 'ajax_object', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );

	// Load Thread comments WordPress script.
	if ( is_singular() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}

add_action( 'wp_enqueue_scripts', 'tema_un_enqueue_scripts', 1 );

// Ocultando a Admin Bar
add_filter( 'show_admin_bar', '__return_false' );


function chromefix_admin_init()
{
  if ( strpos( $_SERVER['HTTP_USER_AGENT'], 'Chrome' ) !== false )
  {
    add_action( 'admin_print_styles', 'chromefix_print_css' );
  }
}

function chromefix_print_css()
{
?>
  <style type="text/css" media="screen">
    #adminmenu {
      transform: translateZ(0);
    }
  </style>
<?php
}

add_action( 'admin_init', 'chromefix_admin_init' );
